class Admin::MembersController < Admin::Base
  # before_action :admin_member, only: [:edit, :update, :destroy]

  def index
    @members = Member.paginate(page: params[:page], per_page: 15)
  end

  def show
    @member = Member.find(params[:id])
    @entries = @member.entries.paginate(page: params[:page], per_page: 3)
    # debugger
  end

  # def new
  #   @member = Member.new
  # end
  #
  # def create
  #   @member = Member.new(member_params)
  #   if @member.save
  #     @member.send_activation_email
  #     flash[:info] = "Please chaeck your email to activate your account."
  #     redirect_to :root
  #   else
  #     render "new"
  #   end
  # end

  # def edit
  #   @member = Member.find(params[:id])
  # end
  #
  # def update
  #   @member = Member.find(params[:id])
  #   if @member.update_attributes(member_params)
  #     flash[:success] = "Profile updated"
  #     redirect_to [:admin, @member], notice: "会員情報を更新しました。"
  #   else
  #     render 'edit'
  #   end
  # end

  def destroy
    Member.find(params[:id]).destroy
    flash[:success] = "Member deleted"
    redirect_to :admin_members, notice: "会員を削除しました。"
  end

  # private
  #
  # def member_params
  #   attrs = [
  #     :new_profile_picture,
  #     :remove_profile_picture,
  #     :name,
  #     :email,
  #     :password,
  #     :password_confirmation,
  #     :profile
  #   ]
  #
  #   attrs << :password if action_name == "create"
  #
  #   params.require(:member).permit(attrs)
  # end
  #
  # def logged_in_member
  #   unless logged_in?
  #     store_location
  #     flash[:danger] = "Please log in."
  #     redirect_to login_url
  #   end
  # end
  #
  # def correct_member
  #     # GET   /users/:id/edit
  #     # PATCH /users/:id
  #     @member = Member.find(params[:id])
  #     redirect_to(root_url) unless current_member?(@member)
  #   end
  #
  # def admin_member
  #   redirect_to(root_url) unless current_member.admin?
  # end
end
