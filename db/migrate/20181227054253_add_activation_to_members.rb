class AddActivationToMembers < ActiveRecord::Migration[5.2]
  def change
    add_column :members, :activation_digest, :string
    add_column :members, :activated, :boolean, defalut: false
    add_column :members, :activated_at, :datetime
  end
end
