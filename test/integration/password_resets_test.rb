require 'test_helper'

class PasswordResetsTest < ActionDispatch::IntegrationTest

  def setup
    ActionMailer::Base.deliveries.clear
    @member = members(:naomi)
  end

  test "password resets" do
    get new_password_reset_path
    assert_template 'password_resets/new'

    post password_resets_path, params: { password_reset: { email: "" } }
    assert_not flash.empty?
    assert_template 'password_resets/new'

    post password_resets_path,
      params: { password_reset: { email: @member.email } }
    assert_not_equal @member.reset_digest, @member.reload.reset_digest
    assert_equal 1, ActionMailer::Base.deliveries.size
    assert_not flash.empty?
    assert_redirected_to :root

    member = assigns(:member)

    get edit_password_reset_path(member.reset_token, email: "")
    assert_redirected_to :root

    member.toggle!(:activated)
    get edit_password_reset_path(member.reset_token, email: member.email)
    assert_redirected_to :root
    member.toggle!(:activated)

    get edit_password_reset_path('wrong token', email: member.email)
    assert_redirected_to :root

    get edit_password_reset_path(member.reset_token, email: member.email)
    assert_template 'password_resets/edit'
    assert_select "input[name=email][type=hidden][value=?]", member.email

    patch password_reset_path(member.reset_token),
        params: { email: member.email,
            member: { password:         "foobaz",
                  password_confirmation: "barquux" } }
    assert_select 'div#error_explanation'

    patch password_reset_path(member.reset_token),
        params: { email: member.email,
            member: { password:          "" } }
    assert_select 'div#error_explanation'

    patch password_reset_path(member.reset_token),
        params: { email: member.email,
            member: { password:         "foobaz",
                  password_confirmation: "foobaz" } }
    assert is_logged_in?
    assert_not flash.empty?
    assert_redirected_to member
  end
end
